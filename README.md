# Teacode task

A simple app to search contacts from external list.

## To run a project on Your Machine:

1. Fork the repository and clone it to Your machine.
2. Please install the dependencies in Your terminal 'npm i'
3. To open the app run 'npm start' in Your terminal
4. Open http://localhost:3000/ in Your browser
