import Contacts from './screens/Contacts';

function App() {
  return (
    <div>
      <Contacts />
    </div>
  );
}

export default App;
