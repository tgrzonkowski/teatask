import React, { useState, useEffect } from 'react';

import Header from '../components/Header';
import List from '../components/List';
import SearchInput from '../components/SearchInput';

const URI =
  'https://teacode-recruitment-challenge.s3.eu-central-1.amazonaws.com/users.json';

const Contacts = () => {
  const [data, setData] = useState(null);
  const [query, setQuery] = useState('');
  const [selectedItems, setSelectedItems] = useState([]);

  const searchItems = (query) => {
    if (!query) {
      return data;
    }
    return data.filter((el) =>
      `${el.first_name.toLowerCase()} ${el.last_name.toLowerCase()}`.includes(
        query.toLowerCase()
      )
    );
  };

  const handleToggleSelectItem = (id) => {
    if (selectedItems.indexOf(id) >= 0) {
      setSelectedItems((prev) => prev.filter((el) => el !== id));
      return;
    }
    setSelectedItems((prev) => [...prev, id]);
  };

  const filteredItems = searchItems(query);

  const fetchData = async () => {
    try {
      const res = await fetch(URI);
      const dataToSave = await res.json();
      setData(dataToSave);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  useEffect(() => {
    console.log(selectedItems);
  }, [selectedItems]);

  return (
    <div>
      <Header />
      <SearchInput value={query} onChange={(e) => setQuery(e.target.value)} />
      {!data && <p>loading data</p>}
      {data && (
        <List
          data={filteredItems.sort((a, b) =>
            a.last_name > b.last_name ? 1 : -1
          )}
          onSelectItem={handleToggleSelectItem}
          selectedItems={selectedItems}
        />
      )}
    </div>
  );
};

export default Contacts;
