import styles from './Avatar.module.scss';

const Avatar = ({ src, alt }) => {
  return (
    <div className={styles.avatar}>
      {!src && <p>{alt}</p>}
      {src && <img src={src} alt="foto"></img>}
    </div>
  );
};

export default Avatar;
