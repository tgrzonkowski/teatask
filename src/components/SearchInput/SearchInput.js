import styles from './SearchInput.module.scss';

const SearchInput = ({ value, onChange }) => {
  return (
    <input
      type="text"
      placeholder="Search"
      value={value}
      onChange={onChange}
      className={styles.input}
    />
  );
};

export default SearchInput;
