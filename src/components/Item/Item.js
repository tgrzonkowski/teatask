import Avatar from '../Avatar';
import styles from './Item.module.scss';

const Item = ({ item, selected, onClick }) => {
  return (
    <li
      className={`${styles.item} ${selected && styles.selected}`}
      onClick={() => onClick(item.id)}
    >
      <div className={styles.container}>
        <Avatar
          src={item.avatar}
          alt={`${item.first_name.charAt(0).toUpperCase()} ${item.last_name
            .charAt(0)
            .toUpperCase()}`}
        />
        <div className={styles.itemData}>
          <p
            className={styles.text}
          >{`${item.first_name} ${item.last_name}`}</p>
          <p className={styles.text}>{item.email}</p>
        </div>
      </div>
      <input
        type="checkbox"
        checked={selected}
        onChange={() => onClick(item.id)}
      />
    </li>
  );
};

export default Item;
