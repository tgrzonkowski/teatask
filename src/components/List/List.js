import Item from '../Item';

const List = ({ data, onSelectItem, selectedItems }) => {
  return (
    <ul>
      {data.map((el) => (
        <Item
          item={el}
          key={el.id}
          onClick={onSelectItem}
          selected={selectedItems.includes(el.id)}
        />
      ))}
    </ul>
  );
};

export default List;
